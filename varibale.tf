variable location {
  default = "west europe"
}

variable resource_group {
  default = "private-endpoint"
}

variable sa_name {
  default = "privatecfm"
}

variable func_name {
  default = "func-p-test"
}

variable "sa_services" {
  default = ["blob", "table", "queue", "file"]
  type    = list
}

variable "sa_firewall_enabled" {
  default = true
  type    = bool
}
variable "enable_static_website" {
  description = "Controls if static website to be enabled on the storage account. Possible values are `true` or `false`"
  default     = true
}

variable "static_website_source_folder" {
  description = "Set a source folder path to copy static website files to static website storage blob"
  default     = "./website1"
}


variable "index_path" {
  description = "path from your repo root to index.html"
  default     = "index.html"
}

variable "custom_404_path" {
  description = "path from your repo root to your custom 404 page"
  default     = "404.html"
}


variable "static_content_directory" {
  type        = string
  default = "./website1"
}

