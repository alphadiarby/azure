locals {
     if_static_website_enabled = var.enable_static_website ? [{}] : []
     temp_dir                  = "${path.root}/api"
     temp_package_zip_path     = "./api.zip"
}
